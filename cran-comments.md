## Test environments

## Resubmission v1.0.1

* Windows 10 Pro, R 4.0.2 OK
* Ubuntu 20.04, R 4.0.2 OK
* win-builder (devel and release) OK
* R-hub Windows Server 2008 R2 SP1, R-devel, 32/64 bit
* R-hub Fedora Linux, R-devel, clang, gfortran
* R-hub Ubuntu Linux 16.04 LTS, R-release, GCC
* R-hub Debian Linux, R-devel, clang, ISO-8859-15 locale

## R CMD check results

There were no ERROR nor WARNING and one NOTE on Windows 10 Pro and Ubuntu 20.04 with R 4.0.2 due to:
NOTE:
* Imports includes 23 non-default packages.
- It seems to be unavoidable and this new version was submitted because CRAN asked for moving packages in Imports, previously in Suggests.
- Besides, spatstat now requires several packages.

There were no ERRORs or WARNINGs and one NOTEs on 'win-builder' on R devel and R-release :
* R-devel: https://win-builder.r-project.org/5a80vT21W01b/00check.log
* R-release: https://win-builder.r-project.org/w7QG5SFbeBVd/00check.log
NOTE:
* checking CRAN incoming feasibility ... NOTE
Maintainer: 'Paul Savary <psavary@protonmail.com>'
- It seems to be unavoidable.


There were no ERRORs or WARNINGs but one NOTEs on:
* R-hub Debian Linux, R-devel, clang, ISO-8859-15 locale 
https://builder.r-hub.io/status/graph4lg_1.2.0.tar.gz-6fe4f987964b472bb9fc8f74ab3d4843

* R-hub Windows Server 2008 R2 SP1, R-devel, 32/64 bit
https://builder.r-hub.io/status/graph4lg_1.2.0.tar.gz-b367a5f1baa64a3b820911e3609d3558

* R-hub macOS 10.13.6 High Sierra, R-release, CRAN's setup 
https://builder.r-hub.io/status/graph4lg_1.2.0.tar.gz-866fe5a05e4e4f2ba4dc2419d6903be0

With the command: rhub::check(env_vars=c(R_COMPILE_AND_INSTALL_PACKAGES = "always")) in all three cases

* checking dependencies in R code ... NOTE
Namespaces in Imports field not imported from:
  All declared Imports should be used.
  'Rdpack' 'knitr' 'rmarkdown' 'spatstat.core'
- This new version was submitted because CRAN asked for moving these packages in Imports, previously in Suggests
- Regarding spatstat, I followed spatstat package maintainers instructions.


There was a PREPERROR:
* With R-hub on Fedora Linux, R-devel, clang, gfortran:
https://builder.r-hub.io/status/graph4lg_1.2.0.tar.gz-2e91cd88600b47a387fe724b6ca6a387

* With R-hub using Ubuntu Linux 16.04 LTS, R-release, GCC: LAUNCHED
https://builder.r-hub.io/status/graph4lg_1.0.1.tar.gz-3ea301c32e5442a5a436bea811e1bafe NOT OK

(same preperror in both cases)
"ERROR: dependencies ‘adegenet’, ‘pegas’, ‘rgdal’, ‘sf’ are not available for package ‘graph4lg’"

These do not seem to be a problem related to the package, but rather a problem due to the package installer on Linux.

## Downstream dependencies

There are currently no downstream dependencies for this package.