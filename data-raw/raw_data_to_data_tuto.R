library(graph4lg)
library(devtools)

data("data_simul_genind")

mat_dps <- mat_gen_dist(x = data_simul_genind, dist = "DPS")

mat_pg <- mat_gen_dist(x = data_simul_genind, dist = "PG")


setwd(dir="D:/psavary/Mes documents/THESE/Analyses/package_cours/graph4lg/data-raw")
dir_def<-"D:/psavary/Mes documents/THESE/Analyses/package_cours/graph4lg/data-raw"



rast <- raster::raster(x = "rast_simul50.tif")

rast <- rast * 1

save(rast, file = "rast_simul50.RDa")


graphab_project(proj_name = "grphb_sim50",
                raster = "rast_simul50.tif",
                habitat = 5)


cost <- data.frame(code = 0:5,
                   cost = c(1, 5, 60, 40, 1000, 1))

graphab_link(proj_name = "grphb_sim50",
             distance = "cost",
             cost = cost,
             name = "lkst1",
             topo = "complete")

graphab_graph(proj_name = "grphb_sim50",
              linkset = "lkst1",
              name = "graph")




graphab_metric(proj_name = "grphb_sim50",
               graph = "graph",
               metric = "F",
               dist = 4000,
               prob = 0.05,
               beta = 1,
               cost_conv = FALSE)

graphab_metric(proj_name = "grphb_sim50",
               graph = "graph",
               metric = "BC",
               dist = 4000,
               prob = 0.05,
               beta = 1,
               cost_conv = FALSE)


graphab_metric(proj_name = "grphb_sim50",
               graph = "graph",
               metric = "IF",
               dist = 4000,
               prob = 0.05,
               beta = 1,
               cost_conv = FALSE)


graphab_metric(proj_name = "grphb_sim50",
               graph = "graph",
               metric = "CCe",
               dist = 4000,
               prob = 0.05,
               beta = 1,
               cost_conv = FALSE)




graphab_modul(proj_name = "grphb_sim50",
              graph = "graph",
              dist = 4000,
              prob = 0.05,
              beta = 1,
              cost_conv = FALSE)


crds_pop <- pts_pop_simul

graphab_pointset(proj_name = "grphb_sim50",
                 linkset = "lkst1",
                 pointset = crds_pop)



get_graphab_linkset(proj_name = "grphb_sim50",
                    linkset = "lkst1")


get_graphab_metric(proj_name = "grphb_sim50")



graphab_to_igraph(proj_name = "grphb_sim50",
                  linkset = "lkst1",
                  nodes = "patches",
                  weight = "cost",
                  fig = TRUE,
                  crds = TRUE)


land_graph <- graphab_to_igraph(proj_name = "grphb_sim50",
                                linkset = "lkst1",
                                nodes = "patches",
                                weight = "cost",
                                fig = FALSE,
                                crds = TRUE)

crds_patches <- land_graph[[2]]
land_graph <- land_graph[[1]]

mat_ld <- as_adjacency_matrix(land_graph, attr = "weight",
                              type = "both", sparse = FALSE)
order <- row.names(mat_ld)[order(c(as.character(row.names(mat_ld))))]
mat_ld <- reorder_mat(mat = mat_ld, order = order)


dmc <- dist_max_corr(mat_gd = mat_dps, mat_ld = mat_ld,
                                   interv = 500, pts_col = "black")

graph_ci <- gen_graph_indep(x = data_simul_genind,
                            dist = "PCA",
                            cov = "sq",
                            adj = "holm")


data_tuto <- list(mat_dps, mat_pg,
                  graph_ci,
                  dmc,
                  land_graph,
                  mat_ld)

names(data_tuto) <- c("mat_dps", "mat_pg", "graph_ci",
                      "dmc", "land_graph", "mat_ld")

usethis::use_data(data_tuto)
