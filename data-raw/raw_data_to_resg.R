setwd(dir="D:/psavary/Mes documents/THESE/Analyses/package_cours/graph4lg/data-raw")
dir_def<-"D:/psavary/Mes documents/THESE/Analyses/package_cours/graph4lg/data-raw"


rast <- raster::raster(x = "rast_simul50.tif")

rast <- rast * 1
save(rast, file = "rast_simul50.RDa")


r.spdf <- as(rast, "SpatialPixelsDataFrame")
r.df <- as.data.frame(r.spdf)

r.df$layer <- as.factor(r.df$rast_simul50)

g <- ggplot(r.df, aes(x=x, y=y)) + geom_tile(aes(fill = layer)) + coord_equal()+
  theme_bw()+
  #scale_fill_brewer(palette="Dark2")+
  scale_fill_manual(values = c("#396D35", "#FB9013", "#EDC951", "#80C342", "black", "#396D35"),
                    labels = c("0 - Forest", "1 - Shrublands", "3 - Crops",
                               "4 - Grasslands","5 - Artificial areas", "6 - Forest"),
                    name = "Land use type")+
  labs(x="Longitude",y="Latitude")
g


#load(file = "rast_rda.RDa")

raster::plot(rast)

load(file = paste0(system.file('extdata', package = 'graph4lg'),
                   "/", "rast_simul50.RDa"))

proj_name <- "graphab_example"

graphab_project(proj_name = proj_name,
                raster = "rast_simul50.tif",
                habitat = c(0, 5),
                minarea = 200)


cost <- data.frame(code = 0:5,
                   cost = c(1, 5, 60, 40, 1000, 1))

cost


graphab_link(proj_name = proj_name,
             distance = "cost",
             cost = cost,
             name = "lkst1",
             topo = "planar")

graphab_graph(proj_name = proj_name,
              linkset = "lkst1",
              name = "graph")



pc <- graphab_metric(proj_name = proj_name,
               graph = "graph",
               metric = "PC",
               dist = 10000,
               prob = 0.05,
               beta = 1,
               cost_conv = TRUE)

pc


f <- graphab_metric(proj_name = proj_name,
                     graph = "graph",
                     metric = "F",
                     dist = 10000,
                     prob = 0.05,
                     beta = 1,
                     cost_conv = TRUE)

f




graphab_modul(proj_name = proj_name,
              graph = "graph",
              dist = 10000,
              prob = 0.05,
              beta = 1)

ptsg <- graphab_pointset(proj_name = proj_name,
                 linkset = "lkst1",
                 pointset = pts_pop_simul)

ptsg

lk <- get_graphab_linkset(proj_name = proj_name,
                          linkset = "lkst1")


met <- get_graphab_metric(proj_name = proj_name)

met

land_graph <- graphab_to_igraph(proj_name = proj_name,
                                linkset = "lkst1",
                                nodes = "patches",
                                weight = "cost",
                                fig = FALSE,
                                crds = TRUE)

crds_patches <- land_graph[[2]]
lgraph <- land_graph[[1]]


plot_graph_lg(land_graph[[1]],
              crds = land_graph[[2]],
              mode = "spatial",
              node_size = "Area")


res_g <- list(pc, f, ptsg, lk, met, crds_patches, lgraph)

names(res_g) <- c("PC", "F", "PTSG", "LK", "MET", "CRDS", "LGRAPH")

save(res_g, file = "res_g.RDa")



